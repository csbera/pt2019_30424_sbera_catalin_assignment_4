package pt.assignment4.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class Observable implements Serializable{
  
  private static final long serialVersionUID = -2368597976907124912L;
  protected List<Observer> observers = new ArrayList<Observer>();

  public void add(Observer observer) {
    if(observers == null) {
      observers = new ArrayList<Observer>();
    }
    observers.add(observer);
  }

  public void remove(Observer observer) {
    if(observers != null)
      observers.remove(observer);
  }

  public void notifyObservers() {
    if(observers != null)
    for (Observer observer : observers)
      observer.update();
  }

}
