package pt.assignment4.business;

import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem {

  private List<MenuItem> components;

  public CompositeProduct(String name, double weight, String description) {
    this.name = name;
    this.weight = weight;
    this.description = description;
  }

  public CompositeProduct(String name, double weight, String description, List<MenuItem> components) {
    this.name = name;
    this.weight = weight;
    this.description = description;
    this.components = new ArrayList<MenuItem>(components);

  }

  public boolean add(MenuItem component) {
    return components.add(component);
  }

  public boolean remove(MenuItem component) {
    return components.remove(component);
  }

  public boolean update(MenuItem component) {
    int index = components.indexOf(component);
    if (index != -1) {
      components.set(index, component);
      return true;
    }
    return false;
  }

  @Override
  public double computePrice() {
    double totalPrice = 5;
    for (MenuItem mi : components) {
      totalPrice += mi.computePrice();
    }
    return totalPrice;
  }

  @Override
  public String toString() {
    String rez = name + " (" + description + "; contains:";
    for (MenuItem m : components) {
      rez += " " + m.getName();
    }
    rez += ") - " + computePrice() + "$ (" + weight + " g)";
    return rez;
  }

}
