package pt.assignment4.business;

import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable{
  private int orderId;
  private Date date;
  private int tableId;

  public Order(int orderId, Date date, int tableId) {
    super();
    this.orderId = orderId;
    this.date = date;
    this.tableId = tableId;
  }

  public int getOrderId() {
    return orderId;
  }

  public void setOrderId(int orderId) {
    this.orderId = orderId;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public int getTableId() {
    return tableId;
  }

  public void setTableId(int tableId) {
    this.tableId = tableId;
  }

  @Override
  public int hashCode() {
    final int prime = 17;
    int result = 1;
    result = prime * result + ((date == null) ? 0 : date.hashCode());
    result = prime * result + orderId;
    result = prime * result + tableId;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Order other = (Order) obj;
    if (date == null) {
      if (other.date != null)
        return false;
    }
    else if (!date.equals(other.date))
      return false;
    if (orderId != other.orderId)
      return false;
    if (tableId != other.tableId)
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Order [orderId=" + orderId + ", date=" + date + ", tableId=" + tableId + "]";
  }

}
