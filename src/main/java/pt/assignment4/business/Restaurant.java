package pt.assignment4.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pt.assignment4.data.FileWriter;

public class Restaurant extends Observable implements Serializable, RestaurantProcessing {

  private static final long serialVersionUID = -2754575687510379601L;
  private List<MenuItem> menu = new ArrayList<MenuItem>();
  private List<Order> orders = new ArrayList<Order>();;
  private Map<Order, List<MenuItem>> operations = new HashMap<Order, List<MenuItem>>();
  private int orderId = 1;
  //public List<Observer> observers = new ArrayList<Observer>();
  
  public Restaurant() {
    observers = new ArrayList<Observer>();
  }

  public boolean createMenuItem(String name, double weight, String description, double price,
      List<MenuItem> components) {
    isWellDefined();

    // Precondition
    try{
      assert (weight > 0 && price > 0);
    } catch(AssertionError e1) {
      System.out.println("Invalid weight/price");
      return false;
    }

    if (components == null || components.isEmpty()) {
      return menu.add(new BaseProduct(name, weight, description, price));
    }
    isWellDefined();
    return menu.add(new CompositeProduct(name, weight, description, components));

  }

  public boolean deleteMenuItem(MenuItem menuItem) {
    isWellDefined();
    return menu.remove(menuItem);
  }

  public boolean updateMenuItem(MenuItem menuItem, String name, double weight, String description, double price,
      List<MenuItem> components) {
    isWellDefined();
    int index = menu.indexOf(menuItem);
    if (index == -1) {
      return false;
    }
    if (components.isEmpty()) {
      menu.set(index, new BaseProduct(name, weight, description, price));
    }
    else {
      menu.set(index, new CompositeProduct(name, weight, description, components));
    }
    isWellDefined();
    return true;
  }

  public boolean createNewOrder(int tableId, List<MenuItem> orderedFood) {
    isWellDefined();
    Order newOrder = new Order(this.orderId++, new Date(), tableId);
    orders.add(newOrder);
    List<MenuItem> food = new ArrayList<MenuItem>(orderedFood);
    notifyObservers();
    isWellDefined();
    return operations.put(newOrder, food) != null;
  }

  public double computePriceForOrder(Order order) {
    isWellDefined();
    List<MenuItem> items = operations.get(order);
    double price = 0;
    System.out.println(items.size() + "cp");
    for (MenuItem mi : items) {
      price += mi.computePrice();
    }

    // PostCondition
    try{
      assert (price > 0);
    } catch(AssertionError e1) {
      System.out.println("Invalid price");
      return 100;
    }
    isWellDefined();
    return price;
  }

  public boolean generateBill(Order order) {
    isWellDefined();
    String filename = String.valueOf(order.hashCode());
    List<MenuItem> items = operations.get(order);
    System.out.println(items.size() + "gb");
    String orderInfo = order.toString() + "\n";
    for (MenuItem mi : items) {
      orderInfo += mi.toString() + "\n";
    }
    orderInfo += "Total Price: " + computePriceForOrder(order);
    isWellDefined();
    return FileWriter.generateBill(orderInfo, filename);
  }

  public Map<Order, List<MenuItem>> getOperations() {
    isWellDefined();
    return operations;
  }

  public List<MenuItem> getMenu() {
    isWellDefined();
    return menu;
  }

  public List<Order> getOrders() {
    isWellDefined();
    return orders;
  }

  public void isWellDefined() {
    // Precondition
    assert (orderId > 0) : "Invalid orderId";
  }

}
