package pt.assignment4.business;

public class BaseProduct extends MenuItem {

  public BaseProduct(String name, double weight, String description, double price) {
    this.name = name;
    this.weight = weight;
    this.description = description;
    this.price = price;
  }

  @Override
  public double computePrice() {
    return price;
  }

  @Override
  public String toString() {
    return name + " (" + description + ") - " + price + "$ (" + weight + " g)";
  }
  
  
  
  

}
