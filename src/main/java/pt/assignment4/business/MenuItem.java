package pt.assignment4.business;

import java.io.Serializable;

public abstract class MenuItem implements Serializable{
  protected String name;
  protected double weight;
  protected String description;
  protected double price;
  
  public abstract double computePrice();

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getWeight() {
    return weight;
  }

  public void setWeight(double weight) {
    this.weight = weight;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
  
  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }
  
}
