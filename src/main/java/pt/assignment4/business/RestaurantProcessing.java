package pt.assignment4.business;

import java.util.List;
import java.util.Map;

public interface RestaurantProcessing {
  boolean createMenuItem(String name, double weight, String description, double price, List<MenuItem> components);

  boolean deleteMenuItem(MenuItem menuItem);

  boolean updateMenuItem(MenuItem menuItem, String name, double weight, String description, double price,
      List<MenuItem> components);

  boolean createNewOrder(int tableId, List<MenuItem> orderedFood);

  double computePriceForOrder(Order order);

  boolean generateBill(Order order);

  Map<Order, List<MenuItem>> getOperations();

  List<MenuItem> getMenu();
}
