package pt.assignment4.business;

import java.io.Serializable;

public interface Observer extends Serializable{

  void update();

}
