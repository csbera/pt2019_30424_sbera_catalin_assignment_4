package pt.assignment4.data;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import pt.assignment4.business.Restaurant;

public class RestaurantSerializer implements Serializable{

  private static final long serialVersionUID = -486947998056260730L;
  private static String filename = "restaurant.ser";
  
  public static void saveRestaurant(Restaurant restaurant) {
    FileOutputStream file = null; 
    ObjectOutputStream out = null;
    try
    {    
        file = new FileOutputStream(filename);
        //System.out.println("1");
        out = new ObjectOutputStream(file); 
        //System.out.println("1");  
        out.writeObject(restaurant); 
        //System.out.println("1");
        
          
        System.out.println("Restaurant has been serialized"); 

    } 
      
    catch(IOException ex) 
    { 
        System.out.println("IOException is caught"); 
    } finally {
      try {
        out.close();
        file.close();
      }
      catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } 
       
    }
  }
  
  public static Restaurant loadRestaurant() {
    Restaurant restaurant = null;
    FileInputStream file = null; 
    ObjectInputStream in = null; 
    try
    {    
        file = new FileInputStream(filename); 
        in = new ObjectInputStream(file); 
          
        restaurant = (Restaurant)in.readObject(); 
          
        in.close(); 
        file.close(); 
          
        System.out.println("Object has been deserialized "); 
    } 
      
    catch(IOException ex) 
    { 
        System.out.println("IOException is caught"); 
    } 
      
    catch(ClassNotFoundException ex) 
    { 
        System.out.println("ClassNotFoundException is caught"); 
    } finally {
      try {
        in.close();
        file.close();
      }
      catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } 
      //file.close();
    }
    return restaurant; 

  }

}
