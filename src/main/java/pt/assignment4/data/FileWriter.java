package pt.assignment4.data;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class FileWriter {
  public static boolean generateBill(String orderInfo, String filename) {
    Writer writer = null;

    try {
      writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream((filename + ".txt"))));
      writer.write(orderInfo);
    }
    catch (IOException ex) {
      return false;
    }
    finally {
      try {
        writer.close();
      }
      catch (IOException e) {
        return false;
      }
    }
    return true;
  }
}
