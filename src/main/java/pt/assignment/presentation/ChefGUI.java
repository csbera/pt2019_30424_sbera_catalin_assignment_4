package pt.assignment.presentation;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JTextPane;

import pt.assignment4.business.Observer;
import pt.assignment4.business.Order;
import pt.assignment4.business.Restaurant;

public class ChefGUI implements Observer {

  private Restaurant observable;
  private List<Order> orders;
  private JFrame frame = new JFrame("Chef GUI");
  private JTextPane ordersText = new JTextPane();

  public ChefGUI(Restaurant observable) {
    this.observable = observable;
    this.observable.add(this);
    
    //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.add(ordersText);
    update();
    frame.setSize(1500, 600);
    //frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  public List<Order> getOrders() {
    return orders;
  }

  public void setOrders(List<Order> orders) {
    this.orders = orders;
  }
  
  public JFrame getFrame() {
    return frame;
  }

  public void update() {
    this.setOrders(observable.getOrders());
    //System.out.println(orders.size());
    ordersText.setText("");
    for(Order order : orders) {
      System.out.println(order.toString());
      ordersText.setText(ordersText.getText() + "\n" + order.toString());
    }
  }

}
