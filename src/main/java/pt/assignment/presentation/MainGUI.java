package pt.assignment.presentation;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.channels.NetworkChannel;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import pt.assignment4.business.Order;
import pt.assignment4.business.Restaurant;
import pt.assignment4.data.RestaurantSerializer;

public class MainGUI {
  
  JFrame frame = new JFrame();
  JFrame user = new JFrame();
  private int frameNr = 0;
  private Restaurant restaurant = new Restaurant();
  private ChefGUI chefG;
  private AdministratorGUI adminG;
  private WaiterGUI waiterG;
  
  public MainGUI() {
    
    frame.setLayout(new GridLayout(2, 1));
    adminG = new AdministratorGUI(restaurant);
    chefG = new ChefGUI(restaurant);
    //restaurant.add(chefG);
    waiterG = new WaiterGUI(restaurant);
    
    JPanel selector = new JPanel();
    selector.setLayout(new GridLayout(1, 3));
    JButton chef = new JButton("Chef");
    JButton waiter = new JButton("Waiter");
    JButton admin = new JButton("Admin");
    selector.add(admin);
    selector.add(waiter);
    selector.add(chef);
    frame.add(selector);
    
    JPanel loader = new JPanel();
    loader.setLayout(new GridLayout(1, 2));
    JButton save = new JButton("Save restaurant config");
    JButton load = new JButton("Load restaurant config");
    loader.add(save);
    loader.add(load);
    frame.add(loader);
    
    save.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        RestaurantSerializer.saveRestaurant(restaurant);
        frame.setTitle("Cofig saved");
      }
    });
    
    load.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        //restaurant.remove(chefG);
        restaurant = RestaurantSerializer.loadRestaurant();
        chefG = new ChefGUI(restaurant);
        waiterG = new WaiterGUI(restaurant);
        adminG = new AdministratorGUI(restaurant);
        user.setVisible(false);
        if(frameNr == 1) user = adminG.getFrame();
        if(frameNr == 2) user = waiterG.getFrame();
        if(frameNr == 3) user = chefG.getFrame();
        user.setVisible(true);
        frame.setTitle("Cofig loaded");
      }
    });
    
    chef.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        frameNr = 3;
        user.setVisible(false);
        restaurant.remove(chefG);
        chefG = new ChefGUI(restaurant);
        user = chefG.getFrame();
        user.setVisible(true);
        frame.setTitle("Chef opened");
      }
    });
    
    admin.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        frameNr = 1;
        user.setVisible(false);
        adminG = new AdministratorGUI(restaurant);
        user = adminG.getFrame();
        user.setVisible(true);
        frame.setTitle("Admin opened");
      }
    });
    
    waiter.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        frameNr = 2;
        user.setVisible(false);
        waiterG = new WaiterGUI(restaurant);
        user = waiterG.getFrame();
        user.setVisible(true);
        frame.setTitle("Waiter opened");
      }
    });
    
    frame.setSize(300, 300);
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }
  
  public static void main(String[] argv) {
    MainGUI m = new MainGUI();
  }

}
