package pt.assignment.presentation;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import pt.assignment4.business.MenuItem;
import pt.assignment4.business.Order;
import pt.assignment4.business.Restaurant;

public class WaiterGUI {
  private Restaurant restaurant;
  public JFrame frame;
  private DefaultListModel<MenuItem> menuItems = new DefaultListModel<MenuItem>();
  private DefaultListModel<Order> orderItems = new DefaultListModel<Order>();
  private List<MenuItem> itemsForOrder = new ArrayList<MenuItem>();
  
  public WaiterGUI(Restaurant restaurant) {
    this.restaurant = restaurant;
    
    frame = new JFrame("Waiter");
    frame.setLayout(new GridLayout(1, 3));
    
    setMenu();
    setOrders();
    
    final JList<MenuItem> menu = new JList<MenuItem>(menuItems);
    frame.add(menu);
    
    JLabel table = new JLabel("Table nr: ");
    final JTextField tableField = new JTextField(10);
    JPanel aux1Panel = new JPanel();
    aux1Panel.setLayout(new FlowLayout());
    aux1Panel.add(table);
    aux1Panel.add(tableField);
    
    JButton addToOrder = new JButton("Add to order");
    JButton createOrder = new JButton("Create order");
    JButton computeBill = new JButton("Compute the bill");
    JPanel auxPanel = new JPanel();
    auxPanel.setLayout(new GridLayout(4, 1));
    auxPanel.add(aux1Panel);
    auxPanel.add(addToOrder);
    auxPanel.add(createOrder);
    auxPanel.add(computeBill);
    frame.add(auxPanel);
    
    final JList<Order> orders = new JList<Order>(orderItems);
    frame.add(orders);
    
    addToOrder.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        MenuItem item = menu.getSelectedValue();
        if(item == null) {
          frame.setTitle("No item selected");
          return;
        }
        itemsForOrder.add(item);
        frame.setTitle("Added menu item");
      }
    });
    
    createOrder.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        if(itemsForOrder.isEmpty()) {
          frame.setTitle("No items to add to order");
          return;
        }
        int tableId;
        try { 
          tableId = Integer.parseInt(tableField.getText());
        } catch(NumberFormatException e1) {
          frame.setTitle("Ivalid table id");
          return;
        }
        if(tableId < 0) {
          frame.setTitle("Ivalid table id");
          return;
        }
        WaiterGUI.this.restaurant.createNewOrder(tableId, itemsForOrder);
        frame.setTitle("Order created");
        itemsForOrder.clear();
        setOrders();
      }
    });
    
    computeBill.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        Order item = orders.getSelectedValue();
        if(item == null) {
          frame.setTitle("No order selected");
          return;
        }
        WaiterGUI.this.restaurant.generateBill(item);
        frame.setTitle("Bill created");
      }
    });
    
    frame.setSize(1500, 600);
    //frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
  }
  
  private void setMenu() {
    List<MenuItem> menu = restaurant.getMenu();
    menuItems.removeAllElements();
    for (MenuItem m : menu) {
      menuItems.addElement(m);
    }
    
  }
  
  private void setOrders() {
    List<Order> menu = restaurant.getOrders();
    orderItems.removeAllElements();
    for (Order m : menu) {
      orderItems.addElement(m);
    }
  }
  
  public JFrame getFrame() {
    return this.frame;
  }
  

}
