package pt.assignment.presentation;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

import pt.assignment4.business.MenuItem;
import pt.assignment4.business.Restaurant;

public class AdministratorGUI {
  private Restaurant restaurant;
  public JFrame frame;
  private DefaultListModel<MenuItem> menuItems = new DefaultListModel<MenuItem>();
  private boolean isBase = true;
  private List<MenuItem> foodComponents = new ArrayList<MenuItem>();

  public AdministratorGUI(Restaurant restaurant) {
    this.restaurant = restaurant;

    setMenu();
//    menuItems.addElement(new BaseProduct("Test", 1, "Test", 1));
//    menuItems.addElement(new BaseProduct("Test", 1, "Test", 1));
//    menuItems.addElement(new BaseProduct("Test", 1, "Test", 1));

    frame = new JFrame("Administrator");
    frame.setLayout(new GridLayout(4, 1));

    JPanel menuItemFrame = new JPanel();
    menuItemFrame.setLayout(new FlowLayout());

    JLabel name = new JLabel("Name: ");
    final JTextField nameField = new JTextField(10);
    menuItemFrame.add(name);
    menuItemFrame.add(nameField);

    final JLabel weight = new JLabel("Weight: ");
    final JTextField weightField = new JTextField(10);
    menuItemFrame.add(weight);
    menuItemFrame.add(weightField);

    JLabel description = new JLabel("Description: ");
    final JTextField descriptionField = new JTextField(20);
    menuItemFrame.add(description);
    menuItemFrame.add(descriptionField);

    JLabel price = new JLabel("Price: ");
    final JTextField priceField = new JTextField(10);
    menuItemFrame.add(price);
    menuItemFrame.add(priceField);

    frame.add(menuItemFrame);

    final JList<MenuItem> components = new JList<MenuItem>(menuItems);
    
    JButton addToComponent = new JButton("Add component");

    frame.add(components);
    frame.add(addToComponent);

    JButton createMenuItem = new JButton("Create menu Item");
    JButton deleteMenuItem = new JButton("Delete menu Item");
    JButton updateMenuItem = new JButton("Update menu Item");
    JPanel auxPanel = new JPanel();
    auxPanel.setLayout(new GridLayout(1, 3));
    auxPanel.add(createMenuItem);
    auxPanel.add(deleteMenuItem);
    auxPanel.add(updateMenuItem);
    frame.add(auxPanel);
    

    
    addToComponent.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        MenuItem item = components.getSelectedValue();
        if(item == null) {
          frame.setTitle("No item selected");
          return;
        }
        isBase = false;
        if(foodComponents.add(item)) {
          frame.setTitle("Added component to menu Item");
        } else {
          frame.setTitle("Could not add component");
        }
      }
    });

    createMenuItem.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        if (nameField.getText().length() == 0 || weightField.getText().length() == 0
            || descriptionField.getText().length() == 0 || priceField.getText().length() == 0) {
          frame.setTitle("Invalid input");
          return;
        }
        if(AdministratorGUI.this.restaurant.createMenuItem(nameField.getText(),
              Double.parseDouble(weightField.getText()), descriptionField.getText(),
              Double.parseDouble(priceField.getText()), foodComponents)) {
          setMenu();

          if (isBase)
            frame.setTitle("Added Base Item");
          else
            frame.setTitle("Added Composite Item");
        } else {
          frame.setTitle("Could not add item");
        }

        setMenu();

        isBase = true;
        foodComponents.clear();
      }
    });
    
    deleteMenuItem.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        MenuItem item = components.getSelectedValue();
        if(item == null) {
          frame.setTitle("No item selected");
          return;
        }
          AdministratorGUI.this.restaurant.deleteMenuItem(item);

        setMenu();

        frame.setTitle("Removed selected menu Item");
        
        isBase = true;
        foodComponents.clear();
      }
    });
    
    updateMenuItem.addActionListener(new ActionListener() {

      public void actionPerformed(ActionEvent e) {
        if (nameField.getText().length() == 0 || weightField.getText().length() == 0
            || descriptionField.getText().length() == 0 || priceField.getText().length() == 0) {
          frame.setTitle("Invalid input");
          return;
        }
        MenuItem item = components.getSelectedValue();
        if(item == null) {
          frame.setTitle("No item selected");
          return;
        }
        System.out.println(foodComponents.size());
          AdministratorGUI.this.restaurant.updateMenuItem(item,nameField.getText(),
              Double.parseDouble(weightField.getText()), descriptionField.getText(),
              Double.parseDouble(priceField.getText()), foodComponents);

        setMenu();

        if (isBase)
          frame.setTitle("Updated to Base Item");
        else
          frame.setTitle("Updated to Composite Item");

        isBase = true;
        foodComponents.clear();
      }
    });
    
    

    frame.setSize(1500, 600);
    //frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

  }
  
  public JFrame getFrame() {
    return this.frame;
  }

  private void setMenu() {
    List<MenuItem> menu = restaurant.getMenu();
    menuItems.removeAllElements();
    for (MenuItem m : menu) {
      menuItems.addElement(m);
    }
  }

}
